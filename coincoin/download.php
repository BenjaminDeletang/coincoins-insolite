<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title>CoinCoins</title>
    <link rel="stylesheet" href="style.scss" />
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhai&display=swap" rel="stylesheet">
    <link rel="icon" href="favicon.ico" />
</head>

<body>
<section class="hero">
    <header>
        <h2 class="company-name">CoinCoins</h2>

        <nav class="mainmenu">
            <ul>
                <li><a href="main.php">Acceuil</a></li>
                <li><a href="download.php" class="active">Telecharger</a></li>
                <li><a href="main.php#contact">Contactez-nous</a></li>
            </ul>
        </nav>
    </header>
    <img class="hero-image" src="image/nothernContour3.jpg" alt="">
    <div class="hero-message">
        <h1>Trouver des paysages insolites à côté de vous.</h1>
    </div>
</section>

<section class="team">
    <div class="row nomargin">
        <h2 class="section_title white">Telecharger L'application</h2>
    </div>
    <div class="downloadImg">
        <img src="image/android.png" alt="">
        <img src="image/apple.png" alt="">
    </div>
</section>

<footer>

    <div class="row row4">
        <div>
            <address>
                <h3>Contact</h3>
                <p>
                    12 rue Vauban <br>
                    Manche<br>
                    Cherbourg-en-Cotentin
                </p>
                <p><a href="mailto:">coincoins@insolite.com</a> </p>
            </address>
        </div>
        <div>
            <h3>Liens</h3>
            <ul>
                <li>
                    <a href="">Terms and Conditions</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li> <a href="">Cookie Policy</a></li>

            </ul>

        </div>
        <div>
            <select>
                <option>Français</option>
                <option>Anglais</option>
            </select>
        </div>
        <div>
            <h3>Social</h3>
            <a href="" class="icon" title="Facebook"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="white" d="M0 0v24h24v-24h-24zm16 7h-1.923c-.616 0-1.077.252-1.077.889v1.111h3l-.239 3h-2.761v8h-3v-8h-2v-3h2v-1.923c0-2.022 1.064-3.077 3.461-3.077h2.539v3z"/></svg></a>
            <!-- Twitter -->
            <a href="" class="icon" title="Twitter"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="white" d="M0 0v24h24v-24h-24zm18.862 9.237c.208 4.617-3.235 9.765-9.33 9.765-1.854 0-3.579-.543-5.032-1.475 1.742.205 3.48-.278 4.86-1.359-1.437-.027-2.649-.976-3.066-2.28.515.098 1.021.069 1.482-.056-1.579-.317-2.668-1.739-2.633-3.26.442.246.949.394 1.486.411-1.461-.977-1.875-2.907-1.016-4.383 1.619 1.986 4.038 3.293 6.766 3.43-.479-2.053 1.079-4.03 3.198-4.03.944 0 1.797.398 2.396 1.037.748-.147 1.451-.42 2.085-.796-.245.767-.766 1.41-1.443 1.816.664-.08 1.297-.256 1.885-.517-.44.656-.997 1.234-1.638 1.697z"/></svg></a>
            <!-- LinkedIn -->
            <a href="" class="icon" title="LinkedIn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="white" d="M0 0v24h24v-24h-24zm8 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.397-2.586 7-2.777 7 2.476v6.759z"/></svg></a>
        </div>
    </div>

</footer>

</body>