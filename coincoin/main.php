<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title>CoinCoins</title>
    <link rel="stylesheet" href="style.scss" />
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhai&display=swap" rel="stylesheet">
    <link rel="icon" href="favicon.ico" />
</head>

<body>
<section class="hero">
    <header>
        <h2 class="company-name">CoinCoins</h2>

        <nav class="mainmenu">
            <ul>
                <li><a href="#" class="active">Acceuil</a></li>
                <li><a href="download.php">Telecharger</a></li>
                <li><a href="#contact">Contactez-nous</a></li>
            </ul>
        </nav>
    </header>
    <img class="hero-image" src="image/nothernContour3.jpg" alt="">
    <div class="hero-message">
        <h1>Trouver des paysages insolites à côté de vous.</h1>
        <h3>Les lieux de l'année</h3>
        <a href="#lieuAnnee" class="btn cta">En savoir plus</a>
    </div>
</section>

<section class="about_us">
    <div class="row nomargin">
        <h2 class="section_title">Le lieu de la semaine</h2>
    </div>
    <div class="row row2">
        <div>
            <h2>Palais du Facteur Cheval</h2>
            <p>Le nom est lui-même un peu étrange. Le site l'est encore davantage. Situé dans le département de la Drôme à Hauterives, le Palais Idéal du Facteur Cheval est une création gigantesque représentant un palais de sculptures d'animaux, de cascades, de temples et d'impressionnants géants. On doit ce site à Ferdinand Cheval, un facteur ayant consacré 33 années de sa vie pour réaliser cet ensemble incroyable !Le palais est aussi bien un hymne à la nature qu'un mélange très personnel de différents styles architecturaux, avec des inspirations puisées tant dans la Bible (grottes de Saint-Amédée et de la Vierge Marie, un calvaire, les évangélistes...) que dans la mythologie hindoue et égyptienne. Ferdinand Cheval est facteur à une époque où se développaient les voyages et la carte postale apparue en France en 1873, cinq ans avant le début du Palais idéal.</p>
        </div>
        <div>
            <figure>
                <img src="image/palais.jpg"alt="">
                <figcaption> Photo prise par Lee Bellule</figcaption>
            </figure>

        </div>
    </div>
</section>

<section class="services">
    <div class="row nomargin">
        <h2 class="section_title">Le lieu inconnu</h2>
    </div>
    <div class="row row4">
        <div class="service">
            <h3> <img src="image/rocher.jpg" alt=""></h3>
        </div>
        <div class="service">
            <p>Les Rochers sculptés de Rothéneuf sont un des environnements spontanés sous forme de sculptures monumentales relevant de l'art brut parmi les plus connus de Bretagne.
                Ils ont été réalisés de fin 1894 à 1907, par l’abbé Adolphe Julien Fouéré, dit l'abbé Fouré (1839-1910).</p>
        </div>
        <div class="service">
            <p>En 1894, il est contraint d’abandonner son poste de recteur à Langouët malgré une pétition de ses paroissiens et de se retirer comme prêtre habitué, à Rothéneuf, à 5 km de Saint-Malo (Rothéneuf se trouve aujourd'hui dans la grande banlieue de cette ville, mais en 1900, était rattaché à la commune de Paramé, station balnéaire alors en vogue).</p>
        </div>
        <div class="service">
            <p>L'ecclésiastique entame alors une œuvre monumentale, directement taillée sur les rochers, fresque sculptée en plein air, à la merci de l'érosion marine. Pendant treize ou quatorze ans, de fin 1894 à 1907, il sculpte plus de 300 statues sur cet ensemble remarquable de rochers granitiques surplombant la mer </p>
        </div>
    </div>
</section>

<section class="team" id="lieuAnnee">
    <div class="row nomargin">
        <h2 class="section_title white">Les lieux insolite de l'année</h2>
    </div>
    <div class="row row4mobile">
        <div class="profile">
            <figure>
                <img src="image/colorado.jpg" alt="">
            </figure>
            <p class="profile_name">Colorado Provençal</p>
            <p class="profile_position">1st place 🥇</p>
            <p>Rustrel 84400</p>
        </div>
        <div class="profile">
            <figure>
                <img src="image/musee.jpg" alt="">
            </figure>
            <p class="profile_name"> Le Musée Robert Tatin</p>
            <p class="profile_position">2nd place 🥈</p>
            <p>Cossé-le-Vivien 53230</p>
        </div>
        <div class="profile">
            <figure>
                <img src="image/france.jpg" alt="">
            </figure>
            <p class="profile_name">France Miniature</p>
            <p class="profile_position">3rd place 🥉</p>
            <p>Élancourt 78990</p>
        </div>
        <div class="profile">
            <figure>
                <img src="image/oriental.jpg" alt="">
            </figure>
            <p class="profile_name">Le Parc Oriental de Maulévrier</p>
            <p class="profile_position">4th place 🏅</p>
            <p>Maulevrier 49360 </p>
        </div>
    </div>
</section>

<section id="contact">
    <div class="row nomargin">
        <h2 class="section_title">Contactez-nous</h2>
    </div>
    <div class="row row2">
        <div>
            <h2>Contactez-nous pour des améliorations, des avis, des envies vous que voudriez retrouver sur notre application</h2>
            <p>Pour ce faire veuillez compléter les champs ci-contre pour nous vous contactions au plus vite</p>
        </div>
        <div>
            <form action="" method="post" class="contact_form">
                <div>
                    <label for="name">Nom / Prénom</label>
                    <input type="text" id="name" name="user_name">
                </div>
                <div>
                    <label for="mail">E-mail</label>
                    <input type="email" id="mail" name="user_mail">
                </div>
                <div>
                    <label for="msg">Message</label>
                    <textarea id="msg" name="user_message" cols="20" rows="5"></textarea>
                </div>
                <div>
                    <button type="submit">Envoyer</button>
                </div>
            </form>

        </div>
    </div>
</section>
<footer>

    <div class="row row4">
        <div>
            <address>
                <h3>Contact</h3>
                <p>
                    12 rue Vauban <br>
                    Manche<br>
                    Cherbourg-en-Cotentin
                </p>
                <p><a href="mailto:">coincoins@insolite.com</a> </p>
            </address>
        </div>
        <div>
            <h3>Liens</h3>
            <ul>
                <li><a href="">Terms and Conditions</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li> <a href="">Cookie Policy</a></li>

            </ul>

        </div>
        <div>
            <h3>Social</h3>
            <a href="" class="icon" title="Facebook"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="white" d="M0 0v24h24v-24h-24zm16 7h-1.923c-.616 0-1.077.252-1.077.889v1.111h3l-.239 3h-2.761v8h-3v-8h-2v-3h2v-1.923c0-2.022 1.064-3.077 3.461-3.077h2.539v3z"/></svg></a>
            <!-- Twitter -->
            <a href="" class="icon" title="Twitter"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="white" d="M0 0v24h24v-24h-24zm18.862 9.237c.208 4.617-3.235 9.765-9.33 9.765-1.854 0-3.579-.543-5.032-1.475 1.742.205 3.48-.278 4.86-1.359-1.437-.027-2.649-.976-3.066-2.28.515.098 1.021.069 1.482-.056-1.579-.317-2.668-1.739-2.633-3.26.442.246.949.394 1.486.411-1.461-.977-1.875-2.907-1.016-4.383 1.619 1.986 4.038 3.293 6.766 3.43-.479-2.053 1.079-4.03 3.198-4.03.944 0 1.797.398 2.396 1.037.748-.147 1.451-.42 2.085-.796-.245.767-.766 1.41-1.443 1.816.664-.08 1.297-.256 1.885-.517-.44.656-.997 1.234-1.638 1.697z"/></svg></a>
            <!-- LinkedIn -->
            <a href="" class="icon" title="LinkedIn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="white" d="M0 0v24h24v-24h-24zm8 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.397-2.586 7-2.777 7 2.476v6.759z"/></svg></a>
        </div>
    </div>

</footer>

<script type="text/javascript" src="main.js"></script>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>